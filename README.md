# <img width="7%" src="https://gitlab.com/toni-lv/freecad-mbdyn-dynamics-workbench/-/raw/master/icons/MBDyn.png" alt="MBDyn-icon"/> FreeCAD-MBDyn dynamics workbench

This project aims at integrating FreeCAD and MBDyn. The objective is to use FreeCAD as pre-post processor for MBDyn, simplifying the process of successfully simulating multibody-dynamics systems and post-processing the simulation results.

## Disclaimer
Please be aware that this software is in an early stage of development. 

## Examples
You will find a range of working examples under the **Examples** folder.
You can see some application examples in my [Youtube channel](https://www.youtube.com/user/josegegas/videos)

These examples are not meant to be **good MBD** simulations. 
Rather, they are meant to be good tests for this software. 
What I test is the behaviour of the **FreeCAD-MBDyn dynamics workbench**,
so please do not use these examples to learn about MBD. 

The **FreeCAD-MBDyn dynamics workbench** will automatically generate an input file for MBDyn,
starting from the CAD model and some FreeCAD **scripted objects**. 

If you wish to learn more about scripted objects you can visit [this site](https://wiki.freecadweb.org/Scripted_objects):

If you wish to learn more about input files for MBDyn you can visit [this link](https://www.sky-engin.jp/en/MBDynTutorial/):

Another basic tool to learn how to write input files is the input file format [can be found here](https://kitefast.readthedocs.io/en/latest/_downloads/f95b077fcef9a3ee4891f17a047c1252/mbdyn-input-1.7.3.pdf)

## Contact
 
If you find this project interesting and you like to program in Python and/or C++, please consider helping with its development.

If you would like to contribute, please contact me at <josegegas@gmail.com>, there is a long list of things to do...

## Installation
Please use this workbench with FreeCAD 0.20.X

This workbench has been tested on Windows 7 and Ubuntu 20.04. If you test it in another system, please let me know.

### Ubuntu Installation
1. Download and Install any Ubuntu Base.
2. After Installing, run `apt-get update`
3. Install the latest updates for gfortran and C++.
4. Download the [MBDyn latest release](https://www.mbdyn.org/?Software_Download)
5. Untar the downloaded file.
6. In the directory of mbdyn[$version], configure the source code by writing the command `./configure --prefix=/usr/local`
7. Make it by writing this command In the directory of mbdyn[$version] `make`
8. Install it by writing this command In the directory of mbdyn[$version] `sudo make install`
9. Download and install FreeCAD 0.20
10. Download the source code of the workbench, extract it and paste it into your Mod folder.
11. Start FreeCAD. The new workbench will be available.

NOTE:

You may need to install mpmath and sympy. Find the instructions to do so next. 
The installation process is the same in both, Windows and Ubuntu.

### Windows Installation

1. Download FreeCAD [0.20.X]( 
https://github.com/FreeCAD/FreeCAD-Bundle/releases/tag/weekly-builds)
2. Extract FreeCAD to any location on your PC.
3. Download [mpmath](https://mpmath.org/)
4. Extract mpmath to any location on your PC.
5. Download [sympy](https://github.com/sympy/sympy/releases)
6. Extract sympy to any location on your PC.
7. In the extracted folder (e.g. **mpmath-1.2.0**) you will find a folder named **mpmath**. 
Copy and paste it in the **bin** folder, which is in "FreeCAD_weekly-builds-24276-Win-Conda_vc14.x-x86_64" 
folder you extracted before.
8. In the extracted folder (e.g. **sympy-1.7.1**) you will find a folder named **sympy**. 
Copy and paste it in the **bin** folder, which is in 
**FreeCAD_weekly-builds-24276-Win-Conda_vc14.x-x86_64** folder you extracted before.
9. Download the **FreeCAD-MBDyn** workbench from this site.
10. Extract the workbench and paste the whole folder (**freecad-mbdyn-dynamics-workbench-master**) into 
the "Mod" folder, which is in "FreeCAD_weekly-builds-24276-Win-Conda_vc14.x-x86_64".
11. Execute FreeCAD. The executable file is in the **bin** folder within **FreeCAD_weekly-builds-24276-Win-Conda_vc14.x-x86_64**, it is **freecad.exe**.
12. That is it, the workbench should now work and you should be able to load the examples I have included with it. 

